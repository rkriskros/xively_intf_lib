/*
 * File Name : gw.h
 * Author : ram krishnan
 * Created : 10/21/2013
 */

#ifndef __GW_H__
#define __GW_H__


// Message type 0x0 is reserved for ACKs
#define LPWMN_GW_MSG_TYPE_NODE_CNT_REQ     0x1
#define LPWMN_GW_MSG_TYPE_NODE_LIST_REQ    0x3

#define LPWMN_GW_MSG_TYPE_RELAY_TO_NODE    0x5
#define LPWMN_GW_MSG_TYPE_RELAY_FROM_NODE  0x6
#define LPWMN_GW_MSG_TYPE_KEEP_ALIVE_REQ   0x7
#define LPWMN_GW_MSG_TYPE_KEEP_ALIVE_RESP  0x8
#define LPWMN_GW_MSG_TYPE_PING_NODE_REQ    0x9
#define LPWMN_GW_MSG_TYPE_NODE_PING_RESP   0xa

#define LPWMN_GW_MSG_TYPE_NODE_RT_DISC_STS   0x10

#define LPWMN_GW_MSG_TYPE_PATH_DISC_REQ    0x13
#define LPWMN_GW_MSG_TYPE_PATH_DISC_RESP   0x14

#define LPWMN_GW_MSG_TYPE_NODE_TRAFFIC_STATS_REQ   0x18

#define LPWMN_GW_MSG_TYPE_RESET_NODE_TRAFFIC_STATS  0x1a

#define LPWMN_GW_MSG_TYPE_STOP_TRAFFIC_REQ   0x1e
#define LPWMN_GW_MSG_TYPE_START_TRAFFIC_REQ  0x1f


#define LPWMN_GW_MSG_TYPE_GET_COORD_STATS    0x20
#define LPWMN_GW_MSG_TYPE_RESET_COORD_STATS  0x21

#define LPWMN_GW_MSG_TYPE_STOP_NWK   0x30
#define LPWMN_GW_MSG_TYPE_START_NWK  0x31

#define LPWMN_GW_MSG_TYPE_DISABLE_NODE_REG  0x40
#define LPWMN_GW_MSG_TYPE_ENABLE_NODE_REG   0x41


#define LPWMN_GW_MSG_TYPE_SET_NWK_PAN_ID          0x50
#define LPWMN_GW_MSG_TYPE_SET_NWK_CHANNEL         0x51
#define LPWMN_GW_MSG_TYPE_SET_PAN_COORD_EXT_ADDR  0x52
#define LPWMN_GW_MSG_TYPE_SET_RADIO_TX_PWR        0x53

#define LPWMN_GW_MSG_TYPE_GET_NWK_PAN_ID          0x59
#define LPWMN_GW_MSG_TYPE_GET_NWK_CHANNEL         0x5a
#define LPWMN_GW_MSG_TYPE_GET_PAN_COORD_EXT_ADDR  0x5b
#define LPWMN_GW_MSG_TYPE_GET_RADIO_TX_PWR        0x5c


#define LPWMN_GW_MSG_TYPE_SET_WCT  0x70   // set wall clock time
#define LPWMN_GW_MSG_TYPE_GET_WCT  0x71   // get wall clock time

#define LPWMN_MSG_TYPE_GET_UP_TIME  0x80  // in seconds


#define LPWMN_GW_MSG_TYPE_COORD_LOG           0x90
#define LPWMN_GW_MSG_TYPE_ENABLE_COORD_LOG    0x91
#define LPWMN_GW_MSG_TYPE_DISABLE_COORD_LOG   0x92

#define LPWMN_GW_MSG_TYPE_DEREG_NODE   0xa0

#define LPWMN_GW_MSG_TYPE_EVENT   0xb0



// Events sent to host by the PAN coordinator
#define LPWMN_GW_EVT_TYPE_NODE_REG               0x1
#define LPWMN_GW_EVT_TYPE_LOCAL_RT_DISC_STARTED  0x2
#define LPWMN_GW_EVT_TYPE_SYS_BOOT  			 0x3

#define LPWMN_GW_EVT_ID_LEN  0x1

#define LPWMN_GW_NODE_CNT_RESP_PYLD_LEN  2

#define LPWMN_GW_MSG_NODE_IDX_FIELD_LEN  2


#define LPWMN_GW_STS_SUCCESS               0x0
#define LPWMN_GW_STS_INV_REQ               0x1
#define LPWMN_GW_STS_INV_PARAMS            0x2
#define LPWMN_GW_STS_PREV_REQ_IN_PROGRESS  0x3
#define LPWMN_GW_STS_NWK_STOPPED           0x4
#define LPWMN_GW_STS_NO_ROUTE_TO_NODE      0x5
#define LPWMN_GW_STS_INV_PYLD_LEN          0x6
#define LPWMN_GW_STS_INV_RADIO_TX_PWR_SET_POINT   0x7


#define LPWMN_MSG_RSSI_LEN  1
#define LPWMN_MSG_CORR_LQI_LEN  1 

#define LPWMN_MAC_SHORT_ADDR_LEN  2
#define LPWMN_MAC_EXT_ADDR_LEN  8
#define LPWMN_MAC_NODE_CAPABILITY_INFO_LEN  1

#define GW_NODE_MAX_CHAN_CNT  32


typedef enum
{
   GW_XIV_CHANN_DATA_TYPE_NA,
   GW_XIV_CHANN_DATA_TYPE_FLOAT_32,
   GW_XIV_CHANN_DATA_TYPE_INT_32
} GW_xivChannDataType_t;


typedef struct
{
   int devId;
   char xivelyChannId[256];  // string
   GW_xivChannDataType_t dataType;
   int lastSnsrVal;
} GW_nodeChannInfo_s;


typedef struct 
{
   char xivDevName[128];
   unsigned char extAddr[LPWMN_MAC_EXT_ADDR_LEN];
   char deviceKeyString[256];
   char feedString[256];

   GW_nodeChannInfo_s nodeChannInfoList[GW_NODE_MAX_CHAN_CNT];

   xi_context_t *xiCntxt_p;

} GW_xivelyDevInfo_s;


typedef struct _LPWMN_nodeListEntry_
{
   unsigned int shortAddr;
   unsigned char extAddr[LPWMN_MAC_EXT_ADDR_LEN];
   unsigned int msgRcvdCnt;
   unsigned int regCnt;

   GW_xivelyDevInfo_s *xivDev_p;
   
   struct _LPWMN_nodeListEntry_ *next_p;
} LPWMN_nodeListEntry_s;


#endif

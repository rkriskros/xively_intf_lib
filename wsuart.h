#ifndef __WSUART_H__
#define __WSUART_H__

#include <termios.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

typedef struct
{
  int baudRate; 
  int serialFd;
  struct termios dcb;
} cntxt_s;

#define flush(uartCntxt_p)  tcflush((uartCntxt_p)->commDevFd, TCIFLUSH)

extern int readPort(unsigned char *buff_p, unsigned int len);
extern int writePort(unsigned char *buff_p, unsigned int cnt);
extern int cfgPort(char *, int);

#endif

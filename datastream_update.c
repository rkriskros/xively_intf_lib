#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include <strings.h>
#include <string.h>
#include <wsuart.h>
#include <xively.h>
#include <xi_helpers.h>
#include <xi_err.h>
#include <dis.h>
#include <gw.h>

// #define XIVELY_TEST

int prevTemp = 0x0;
int verbose = 0x0;
int sendToXively = 0x1;
   
unsigned int GW_rcvdMsgCnt = 0;

LPWMN_nodeListEntry_s  *LPWMN_nodeListHead_p = NULL;

GW_xivelyDevInfo_s GW_xivelyInfoList[ ] = 
{
   {
      "Node_1",
      {0xfc, 0xc2, 0x3d, 0x0, 0x0, 0x0, 0xdc, 0xfd},
      "XZ5VlJQ8L0EVKMFHr14oUe90ODp6fBk8NupNF0adoniJub9w",
      "224091213",
      {

         {0x09, "Temp_lm75b_1",   GW_XIV_CHANN_DATA_TYPE_FLOAT_32, 0x10000}, // Temperature
         {0x12, "Light_TSL45315_1", GW_XIV_CHANN_DATA_TYPE_INT_32, 0xf0000},    // Ambient light sensor
#if 0
         {0x90, "KeyInSlotInd",   GW_XIV_CHANN_DATA_TYPE_INT_32, 0xa5}, // Binary 
         {0x91, "CoolingSig",     GW_XIV_CHANN_DATA_TYPE_INT_32, 0xa5}, // Binary 
         {0xb8, "P_MPL3115A2_1",  GW_XIV_CHANN_DATA_TYPE_INT_32, 0x0},  // Pressure
         {0xb0, "RH_CC2D33S",     GW_XIV_CHANN_DATA_TYPE_INT_32, 0x0},  // RH
         {0xc0, "Shake",          GW_XIV_CHANN_DATA_TYPE_INT_32, 0x0},  // Back / front
#endif
         {0x78, "Vcc_MSP430_1",   GW_XIV_CHANN_DATA_TYPE_FLOAT_32, 0x0}, // Temperature
         {0xfe, "RSSI_9",           GW_XIV_CHANN_DATA_TYPE_INT_32, 1024},  // RSSI
         {0x0,  "",               GW_XIV_CHANN_DATA_TYPE_NA, 0x0}  // end of list
      },
      NULL
   },

#if 1
   {
      "Node_2",
      {0xfc, 0xc2, 0x3d, 0x0, 0x0, 0x0, 0xdc, 0xef},
      "FyKBl4ySbXlgE9e7nzNUcT5Q3TVjj16TF7UwMiHYjEqvZvAc",
      "1279490255",
      {

         {0x09, "Temp_lm75b_1",   GW_XIV_CHANN_DATA_TYPE_FLOAT_32, 0x10000}, // Temperature
         {0x12, "Light_tsl45315_1", GW_XIV_CHANN_DATA_TYPE_INT_32, 0xffffff},    // Ambient light sensor
#if 0
         {0x90, "KeyInSlotInd",   GW_XIV_CHANN_DATA_TYPE_INT_32, 0xa5}, // Binary 
         {0x91, "CoolingSig",     GW_XIV_CHANN_DATA_TYPE_INT_32, 0xa5}, // Binary 
         {0x12, "ALS_TSL45315_1", GW_XIV_CHANN_DATA_TYPE_INT_32},    // Ambient light sensor
         {0xb8, "P_MPL3115A2_1",  GW_XIV_CHANN_DATA_TYPE_INT_32},  // Pressure
         {0xb0, "RH_CC2D33S",     GW_XIV_CHANN_DATA_TYPE_INT_32},  // RH
         {0xc0, "Shake",    GW_XIV_CHANN_DATA_TYPE_INT_32},  // Back / front
#endif
         {0x78, "Vcc_MSP430_1",   GW_XIV_CHANN_DATA_TYPE_FLOAT_32, 0x0}, // Temperature
         {0xfe, "RSSI_10",        GW_XIV_CHANN_DATA_TYPE_INT_32, 1024},  // RSSI
         {0x0,  "",               GW_XIV_CHANN_DATA_TYPE_NA, 0x0}  // end of list
      },
      NULL
   },
#endif

#if 1
   {
      "Node_3",
      {0xfc, 0xc2, 0x3d, 0x0, 0x0, 0x0, 0xe3, 0x5c},
      "8KX1wb892FXHh8mDvHEZRtYUSp6YukBNZ7BBpxrs5uspCEZK",
      "1275239525",
      {
         {0x09, "Temp_lm75b_1",   GW_XIV_CHANN_DATA_TYPE_FLOAT_32, 0x10000}, // Temperature
         {0x12, "Light_tsl45315_1", GW_XIV_CHANN_DATA_TYPE_INT_32, 0xffffff},    // Ambient light sensor
#if 0
         {0x90, "KeyInSlotInd",   GW_XIV_CHANN_DATA_TYPE_INT_32, 0xa5}, // Binary 
         {0x91, "CoolingSig",     GW_XIV_CHANN_DATA_TYPE_INT_32, 0xa5}, // Binary 
         {0x12, "ALS_TSL45315_1", GW_XIV_CHANN_DATA_TYPE_INT_32},    // Ambient light sensor
         {0xb8, "P_MPL3115A2_1",  GW_XIV_CHANN_DATA_TYPE_INT_32},  // Pressure
         {0xb0, "RH_CC2D33S",     GW_XIV_CHANN_DATA_TYPE_INT_32},  // RH
         {0xc0, "Shake",    GW_XIV_CHANN_DATA_TYPE_INT_32},  // Back / front
#endif
         {0x78, "Vcc_MSP430_1",   GW_XIV_CHANN_DATA_TYPE_FLOAT_32, 0x0}, // Temperature
         {0xfe, "RSSI_11",        GW_XIV_CHANN_DATA_TYPE_INT_32, 1024},  // RSSI
         {0x0,  "",               GW_XIV_CHANN_DATA_TYPE_NA, 0x0}  // end of list
      },
      NULL
   },
#endif

#if 1
   {
      "Node_4",
      {0xfc, 0xc2, 0x3d, 0x0, 0x0, 0x0, 0xe8, 0xa9},
      "",
      "",
      {
         {0x09, "Temp_lm75b_1",   GW_XIV_CHANN_DATA_TYPE_FLOAT_32, 0x10000}, // Temperature
         {0x12, "Light_tsl45315_1", GW_XIV_CHANN_DATA_TYPE_INT_32, 0xffffff},    // Ambient light sensor
#if 0
         {0x90, "KeyInSlotInd",   GW_XIV_CHANN_DATA_TYPE_INT_32, 0xa5}, // Binary 
         {0x91, "CoolingSig",     GW_XIV_CHANN_DATA_TYPE_INT_32, 0xa5}, // Binary 
         {0x12, "ALS_TSL45315_1", GW_XIV_CHANN_DATA_TYPE_INT_32},    // Ambient light sensor
         {0xb8, "P_MPL3115A2_1",  GW_XIV_CHANN_DATA_TYPE_INT_32},  // Pressure
         {0xb0, "RH_CC2D33S",     GW_XIV_CHANN_DATA_TYPE_INT_32},  // RH
         {0xc0, "Shake",    GW_XIV_CHANN_DATA_TYPE_INT_32},  // Back / front
#endif
         {0x78, "Vcc_MSP430_1",   GW_XIV_CHANN_DATA_TYPE_FLOAT_32, 0x0}, // Temperature
         {0xfe, "RSSI_11",        GW_XIV_CHANN_DATA_TYPE_INT_32, 1024},  // RSSI
         {0x0,  "",               GW_XIV_CHANN_DATA_TYPE_NA, 0x0}  // end of list
      },
      NULL
   },
#endif

#if 1
   {
      "Node_5",
      {0xfc, 0xc2, 0x3d, 0x0, 0x0, 0x0, 0xe9, 0x54},
      "",
      "",
      {
         {0x09, "Temp_lm75b_1",   GW_XIV_CHANN_DATA_TYPE_FLOAT_32, 0x10000}, // Temperature
         {0x12, "Light_tsl45315_1", GW_XIV_CHANN_DATA_TYPE_INT_32, 0xffffff},    // Ambient light sensor
#if 0
         {0x90, "KeyInSlotInd",   GW_XIV_CHANN_DATA_TYPE_INT_32, 0xa5}, // Binary 
         {0x91, "CoolingSig",     GW_XIV_CHANN_DATA_TYPE_INT_32, 0xa5}, // Binary 
         {0x12, "ALS_TSL45315_1", GW_XIV_CHANN_DATA_TYPE_INT_32},    // Ambient light sensor
         {0xb8, "P_MPL3115A2_1",  GW_XIV_CHANN_DATA_TYPE_INT_32},  // Pressure
         {0xb0, "RH_CC2D33S",     GW_XIV_CHANN_DATA_TYPE_INT_32},  // RH
         {0xc0, "Shake",    GW_XIV_CHANN_DATA_TYPE_INT_32},  // Back / front
#endif
         {0x78, "Vcc_MSP430_1",   GW_XIV_CHANN_DATA_TYPE_FLOAT_32, 0x0}, // Temperature
         {0xfe, "RSSI_11",        GW_XIV_CHANN_DATA_TYPE_INT_32, 1024},  // RSSI
         {0x0,  "",               GW_XIV_CHANN_DATA_TYPE_NA, 0x0}  // end of list
      },
      NULL
   },
#endif

#if 1
   {
      "Node_6",
      {0xfc, 0xc2, 0x3d, 0x0, 0x0, 0x0, 0xf7, 0x71},
      "",
      "",
      {
         {0x09, "Temp_lm75b_1",   GW_XIV_CHANN_DATA_TYPE_FLOAT_32, 0x10000}, // Temperature
         {0x12, "Light_tsl45315_1", GW_XIV_CHANN_DATA_TYPE_INT_32, 0xffffff},    // Ambient light sensor
#if 0
         {0x90, "KeyInSlotInd",   GW_XIV_CHANN_DATA_TYPE_INT_32, 0xa5}, // Binary 
         {0x91, "CoolingSig",     GW_XIV_CHANN_DATA_TYPE_INT_32, 0xa5}, // Binary 
         {0x12, "ALS_TSL45315_1", GW_XIV_CHANN_DATA_TYPE_INT_32},    // Ambient light sensor
         {0xb8, "P_MPL3115A2_1",  GW_XIV_CHANN_DATA_TYPE_INT_32},  // Pressure
         {0xb0, "RH_CC2D33S",     GW_XIV_CHANN_DATA_TYPE_INT_32},  // RH
         {0xc0, "Shake",    GW_XIV_CHANN_DATA_TYPE_INT_32},  // Back / front
#endif
         {0x78, "Vcc_MSP430_1",   GW_XIV_CHANN_DATA_TYPE_FLOAT_32, 0x0}, // Temperature
         {0xfe, "RSSI_11",        GW_XIV_CHANN_DATA_TYPE_INT_32, 1024},  // RSSI
         {0x0,  "",               GW_XIV_CHANN_DATA_TYPE_NA, 0x0}  // end of list
      },
      NULL
   },
#endif

#if 1
   {
      "Node_7",
      {0xfc, 0xc2, 0x3d, 0x0, 0x0, 0x0, 0xdd, 0x0c},
      "",
      "",
      {
         {0x09, "Temp_lm75b_1",   GW_XIV_CHANN_DATA_TYPE_FLOAT_32, 0x10000}, // Temperature
         {0x12, "Light_tsl45315_1", GW_XIV_CHANN_DATA_TYPE_INT_32, 0xffffff},    // Ambient light sensor
#if 0
         {0x90, "KeyInSlotInd",   GW_XIV_CHANN_DATA_TYPE_INT_32, 0xa5}, // Binary 
         {0x91, "CoolingSig",     GW_XIV_CHANN_DATA_TYPE_INT_32, 0xa5}, // Binary 
         {0x12, "ALS_TSL45315_1", GW_XIV_CHANN_DATA_TYPE_INT_32},    // Ambient light sensor
         {0xb8, "P_MPL3115A2_1",  GW_XIV_CHANN_DATA_TYPE_INT_32},  // Pressure
         {0xb0, "RH_CC2D33S",     GW_XIV_CHANN_DATA_TYPE_INT_32},  // RH
         {0xc0, "Shake",    GW_XIV_CHANN_DATA_TYPE_INT_32},  // Back / front
#endif
         {0x78, "Vcc_MSP430_1",   GW_XIV_CHANN_DATA_TYPE_FLOAT_32, 0x0}, // Temperature
         {0xfe, "RSSI_11",        GW_XIV_CHANN_DATA_TYPE_INT_32, 1024},  // RSSI
         {0x0,  "",               GW_XIV_CHANN_DATA_TYPE_NA, 0x0}  // end of list
      },
      NULL
   },
#endif

#if 1
   {
      "Node_8",
      {0xfc, 0xc2, 0x3d, 0x0, 0x0, 0x0, 0xdd, 0x32},
      "",
      "",
      {
         {0x09, "Temp_lm75b_1",   GW_XIV_CHANN_DATA_TYPE_FLOAT_32, 0x10000}, // Temperature
         {0x12, "Light_tsl45315_1", GW_XIV_CHANN_DATA_TYPE_INT_32, 0xffffff},    // Ambient light sensor
#if 0
         {0x90, "KeyInSlotInd",   GW_XIV_CHANN_DATA_TYPE_INT_32, 0xa5}, // Binary 
         {0x91, "CoolingSig",     GW_XIV_CHANN_DATA_TYPE_INT_32, 0xa5}, // Binary 
         {0x12, "ALS_TSL45315_1", GW_XIV_CHANN_DATA_TYPE_INT_32},    // Ambient light sensor
         {0xb8, "P_MPL3115A2_1",  GW_XIV_CHANN_DATA_TYPE_INT_32},  // Pressure
         {0xb0, "RH_CC2D33S",     GW_XIV_CHANN_DATA_TYPE_INT_32},  // RH
         {0xc0, "Shake",    GW_XIV_CHANN_DATA_TYPE_INT_32},  // Back / front
#endif
         {0x78, "Vcc_MSP430_1",   GW_XIV_CHANN_DATA_TYPE_FLOAT_32, 0x0}, // Temperature
         {0xfe, "RSSI_11",        GW_XIV_CHANN_DATA_TYPE_INT_32, 1024},  // RSSI
         {0x0,  "",               GW_XIV_CHANN_DATA_TYPE_NA, 0x0}  // end of list
      },
      NULL
   },
#endif

#if 1
   {
      "Node_7",
      {0xfc, 0xc2, 0x3d, 0x0, 0x0, 0x0, 0xdd, 0x17},
      "",
      "",
      {
         {0x09, "Temp_lm75b_1",   GW_XIV_CHANN_DATA_TYPE_FLOAT_32, 0x10000}, // Temperature
         {0x12, "Light_tsl45315_1", GW_XIV_CHANN_DATA_TYPE_INT_32, 0xffffff},    // Ambient light sensor
#if 0
         {0x90, "KeyInSlotInd",   GW_XIV_CHANN_DATA_TYPE_INT_32, 0xa5}, // Binary 
         {0x91, "CoolingSig",     GW_XIV_CHANN_DATA_TYPE_INT_32, 0xa5}, // Binary 
         {0x12, "ALS_TSL45315_1", GW_XIV_CHANN_DATA_TYPE_INT_32},    // Ambient light sensor
         {0xb8, "P_MPL3115A2_1",  GW_XIV_CHANN_DATA_TYPE_INT_32},  // Pressure
         {0xb0, "RH_CC2D33S",     GW_XIV_CHANN_DATA_TYPE_INT_32},  // RH
         {0xc0, "Shake",    GW_XIV_CHANN_DATA_TYPE_INT_32},  // Back / front
#endif
         {0x78, "Vcc_MSP430_1",   GW_XIV_CHANN_DATA_TYPE_FLOAT_32, 0x0}, // Temperature
         {0xfe, "RSSI_11",        GW_XIV_CHANN_DATA_TYPE_INT_32, 1024},  // RSSI
         {0x0,  "",               GW_XIV_CHANN_DATA_TYPE_NA, 0x0}  // end of list
      },
      NULL
   },
#endif
};


/*
 * Rcvd message format - 
 * Msg type (2 bytes)
 * Flags (1 byte)
 * Seq Nr (1 byte)
 * Pyld len (2 bytes)
 * Hdr crc (2 bytes)
 * Pyld crc (2 bytes)
 * Pyld (N bytes)
 */

#define UART_MSG_HDR_MSG_TYPE_FIELD_LEN  2
#define UART_MSG_HDR_FLAGS_FIELD_LEN     1
#define UART_MSG_HDR_SEQ_NR_FIELD_LEN    1
#define UART_MSG_HDR_PYLD_LEN_FIELD_LEN  2
#define UART_MSG_HDR_HDR_CRC_FIELD_LEN   2
#define UART_MSG_HDR_PYLD_CRC_FIELD_LEN  2


#define UART_MSG_HDR_MSG_TYPE_FIELD_OFF  0
#define UART_MSG_HDR_FLAGS_FIELD_OFF     2
#define UART_MSG_HDR_SEQ_NR_FIELD_OFF    3
#define UART_MSG_HDR_PYLD_LEN_FIELD_OFF  4
#define UART_MSG_HDR_HDR_CRC_FIELD_OFF   6
#define UART_MSG_HDR_PYLD_CRC_FIELD_OFF  8

#define UART_MSG_HDR_LEN (UART_MSG_HDR_PYLD_CRC_FIELD_OFF + UART_MSG_HDR_PYLD_CRC_FIELD_LEN)



// TLV management
#define TLV_TYPE_FIELD_SZ   0x1
#define TLV_LEN_FIELD_SZ    0x1

#define TLV_HDR_SZ  \
        (TLV_TYPE_FIELD_SZ + TLV_LEN_FIELD_SZ)


#define SER_INPUT_BUFF_LEN  1024

unsigned char serInputBuff[SER_INPUT_BUFF_LEN];

char battV_dsId[ ] = "battVoltage";
char msp430Temp_dsId[ ] = "TempSnsr_msp430";
char tmp102Temp_dsId[ ] = "TempSnsr_tmp102";
char als_dsId[ ] = "LightSensor_tsl45315";
char llSnsr_dsId[ ] = "OvhdTankWL_mp3v5050";
char LoadSnsr_dsId[ ] = "Weight";


#define RX_TIMEOUT_SEC 1

static cntxt_s uart_cntxt;


#define POLY 0x1021  // 0x8408
/*
//                                      16   12   5
// this is the CCITT CRC 16 polynomial X  + X  + X  + 1.
// This works out to be 0x1021, but the way the algorithm works
// lets us use 0x8408 (the reverse of the bit pattern).  The high
// bit is always assumed to be set, thus we only use 16 bits to
// represent the 17 bit value.
*/


/*
 ********************************************************************
 *
 *
 *
 *
 ********************************************************************
 */
unsigned short crc16(unsigned char *buff_p, unsigned char len)
{
   unsigned int ckSum = 0;

   while (len > 1)
   {
      unsigned short tmp = *buff_p;
      tmp = (tmp << 8) | (*(buff_p + 1));
      ckSum = ckSum + tmp;
      buff_p += 2;
      len -= 2;
   }

   if (len > 0)
       ckSum += (*buff_p);

   while (ckSum >> 16)
   {
      ckSum = (ckSum & 0xffff) + (ckSum >> 16);
   }

   return (~ckSum);
}


/*
 ********************************************************************
 *
 *
 *
 *
 ********************************************************************
 */
LPWMN_nodeListEntry_s *GW_allocNode(void)
{
   LPWMN_nodeListEntry_s *new_p = (LPWMN_nodeListEntry_s *)malloc(sizeof(LPWMN_nodeListEntry_s));
   if (new_p == NULL)
   {
       printf("\n**************malloc() failed !!**************\n");
       exit(1);
   }
  
   memset(new_p, 0, sizeof(LPWMN_nodeListEntry_s));


   if (LPWMN_nodeListHead_p != NULL)
       new_p->next_p = LPWMN_nodeListHead_p;
   else
       new_p->next_p = NULL;
       
   LPWMN_nodeListHead_p = new_p;

   return new_p;
}


/*
 ********************************************************************
 *
 *
 *
 *
 ********************************************************************
 */
LPWMN_nodeListEntry_s *GW_lookUpExtAddr(unsigned char *extAddr_p)
{
   LPWMN_nodeListEntry_s *node_p = LPWMN_nodeListHead_p;

   while (node_p != NULL)
   {
      if (memcmp(extAddr_p, node_p->extAddr, LPWMN_MAC_EXT_ADDR_LEN) == 0x0)
          break;
      else
      {
          node_p = node_p->next_p;
      }
   }

   return node_p;
}


/*
 ********************************************************************
 *
 *
 *
 *
 ********************************************************************
 */
LPWMN_nodeListEntry_s *GW_lookUpShortAddr(unsigned int shortAddr)
{
   LPWMN_nodeListEntry_s *node_p = LPWMN_nodeListHead_p;

   while (node_p != NULL)
   {
      if (node_p->shortAddr == shortAddr)
          break;
      else
      {
          node_p = node_p->next_p;
      }
   }

   return node_p;
}


/*
 ********************************************************************
 *
 *
 *
 *
 ********************************************************************
 */
void print_usage()
{
    printf("Usage: ./datastream_update  <serial_device> \n");
    printf("Example:  ./datastream_update /dev/ttyUSB0 \n");
}


/*
 ********************************************************************
 *
 *
 *
 *
 ********************************************************************
 */
unsigned short __ntohs(unsigned char *buff_p)
{
   short u16Val = *buff_p;
   u16Val = (u16Val << 8) | buff_p[1];  
   return u16Val;
}


/*
 ********************************************************************
 *
 *
 *
 *
 ********************************************************************
 */
unsigned int __ntohl(unsigned char *buff_p)
{
   unsigned int u32Val = *buff_p;
   u32Val <<= 8;
   u32Val |= buff_p[1];
   u32Val <<= 8;
   u32Val |= buff_p[2];
   u32Val <<= 8;
   u32Val |= buff_p[3];
   return u32Val;
}


/*
 ********************************************************************
 *
 *
 *
 *
 ********************************************************************
 */
unsigned char TLV_get(unsigned char *buff_p, unsigned char len, unsigned char type,
                       unsigned char *pyldLen_p, unsigned char **pyldBuff_pp)
{
    int buffLen = len;
    unsigned char rc = 0;

    if (buffLen < TLV_HDR_SZ)
        return 0;

    // Get the tlv type

    while (buffLen >= TLV_HDR_SZ)
    {
        unsigned char tlvPyldLen = *(buff_p + TLV_TYPE_FIELD_SZ);

        if (*buff_p == type)
        {
            *pyldLen_p = tlvPyldLen;
            *pyldBuff_pp = (buff_p + TLV_HDR_SZ);
            rc = 1;
            break;
        }
        else
        {
            buff_p += (TLV_HDR_SZ + tlvPyldLen);
            buffLen -= (TLV_HDR_SZ + tlvPyldLen);
        }
    }

    return rc;
}


/*
 ********************************************************************
 *
 *
 *
 *
 ********************************************************************
 */
int readPort(unsigned char *buff_p, unsigned int len)
{
   int rdLen, readLeft = len, totRead = 0;

   while (readLeft > 0)
   {
      rdLen = read(uart_cntxt.serialFd, buff_p + totRead, readLeft);
      if (rdLen > 0)
      {
          totRead += rdLen;
          readLeft -= rdLen;
      }
      else
      {
    	  printf("<%s> read() failed  - %d !! \n", __FUNCTION__, rdLen);
          return rdLen;
      }
   }

   return totRead;
}

/*
 ********************************************************************
 *
 *
 *
 *
 ********************************************************************
 */
int writePort(unsigned char *buff_p, unsigned int cnt)
{
   int rc, bytesLeft = cnt, bytesWritten = 0;
   
   if (verbose) 
       printf("<%s> cnt<%d> \n", __FUNCTION__, cnt);
   
   while (bytesLeft > 0)
   {
      rc = write(uart_cntxt.serialFd, buff_p + bytesWritten, bytesLeft);
      if (rc <= 0)
          return -1;
      else
      {
          bytesLeft -= rc;
          bytesWritten += rc;
      }
   }

   return 1;
}


#ifdef __CYGWIN__
    

/*
 ********************************************************************
 *
 *
 *
 *
 ********************************************************************
 */
int cfgPort(char *serDevName_p, int baudRate)
{
    struct termios newtio;
    struct termios oldtio;
    struct termios latesttio;
    cntxt_s *serialCntxt_p = &uart_cntxt;
    int rc;

    serialCntxt_p->serialFd = open(serDevName_p, O_RDWR | O_NOCTTY );
    if (serialCntxt_p->serialFd < 0)
    {
        printf("\n open failed .... \n");
        return -1;
    }

    rc = tcgetattr(serialCntxt_p->serialFd, &oldtio); /* save current port settings */
    if (rc < 0)
    {
        printf("\n tcgetattr() failed !! - rc<%d>, errno<%d> \n", rc, errno);
        return -1;
    }

    bzero(&newtio, sizeof(newtio));

    rc = cfsetspeed(&newtio, baudRate);
    if (rc < 0)
    {
        printf("\n cfsetspeed() failed !! - rc<%d>, errno<%d> \n", rc, errno);
        return -1;
    }

    newtio.c_cflag = CS8 | CLOCAL | CREAD;
    newtio.c_iflag = IGNPAR;
    newtio.c_oflag = 0;

    // if ((rc = fcntl(serialCntxt_p->serialFd, F_SETOWN, getpid())) < 0)
    // {
    //     printf("\n fcntl failed !! - rc<%d>, errno<%d> \n", rc, errno);
    //     return -1;
    // }

    /* set input mode (non-canonical, no echo,...) */
    newtio.c_lflag = 0;

    newtio.c_cc[VTIME]    = 0;   /* inter-character timer unused */
    newtio.c_cc[VMIN]     = 1;   /* blocking read until 10 chars received */

    rc = tcsetattr(serialCntxt_p->serialFd, TCSANOW, &newtio);
    if (rc < 0)
    {
        printf("\n tcsetattr() failed !! - rc<%d> / errno<%d> \n", rc, errno);
        return -1;
    }

    rc = tcflush(serialCntxt_p->serialFd, TCIFLUSH);
    if (rc < 0)
    {
        printf("\n tcflush() failed !! - rc<%d> \n", rc);
        return -1;
    }
    
    tcgetattr(serialCntxt_p->serialFd, &latesttio); 
    if (rc < 0)
    {
        printf("\n tcgetattr() failed !! - rc<%d> \n", rc);
        return -1;
    }

    printf("\nispeed<%d> / ospeed<%d> \n", latesttio.c_ispeed, latesttio.c_ospeed);
    printf("\niflag<0x%x>/oflag<0x%x>/cflag<0x%x> \n", latesttio.c_iflag, latesttio.c_oflag, latesttio.c_cflag);

    return 1;
}



#else 
                             
/*
 ********************************************************************
 *
 *
 *
 *
 ********************************************************************
 */
int cfgPort(char *serDevName_p, int baudRate)
{
   cntxt_s *serialCntxt_p = &uart_cntxt;
   int rc;

   memset(serialCntxt_p, 0, sizeof(cntxt_s));

   serialCntxt_p->serialFd = open((char *)serDevName_p, O_RDWR | O_NOCTTY | O_NDELAY);
   if (serialCntxt_p->serialFd < 0)
   {
       printf("\n<%s> open(%s) failed !! - errno<%d> \n",
              __FUNCTION__, serDevName_p, errno);
       return -1;
   }
   
   // Zero out port status flags
   if (fcntl(serialCntxt_p->serialFd, F_SETFL, 0) != 0x0)
   {
       return -1;
   }

   bzero(&(serialCntxt_p->dcb), sizeof(serialCntxt_p->dcb));

   // serialCntxt_p->dcb.c_cflag |= serialCntxt_p->baudRate;  // Set baud rate first time
   serialCntxt_p->dcb.c_cflag |= baudRate;  // Set baud rate first time
   serialCntxt_p->dcb.c_cflag |= CLOCAL;  // local - don't change owner of port
   serialCntxt_p->dcb.c_cflag |= CREAD;  // enable receiver

   // Set to 8N1
   serialCntxt_p->dcb.c_cflag &= ~PARENB;  // no parity bit
   serialCntxt_p->dcb.c_cflag &= ~CSTOPB;  // 1 stop bit
   serialCntxt_p->dcb.c_cflag &= ~CSIZE;  // mask character size bits
   serialCntxt_p->dcb.c_cflag |= CS8;  // 8 data bits

   // Set output mode to 0
   serialCntxt_p->dcb.c_oflag = 0;
 
   serialCntxt_p->dcb.c_lflag &= ~ICANON;  // disable canonical mode
   serialCntxt_p->dcb.c_lflag &= ~ECHO;  // disable echoing of input characters
   serialCntxt_p->dcb.c_lflag &= ~ECHOE;
 
   // Set baud rate
   cfsetispeed(&serialCntxt_p->dcb, serialCntxt_p->baudRate);
   cfsetospeed(&serialCntxt_p->dcb, serialCntxt_p->baudRate);

   serialCntxt_p->dcb.c_cc[VTIME] = 0;  // timeout = 0.1 sec
   serialCntxt_p->dcb.c_cc[VMIN] = 1;
 
   if ((tcsetattr(serialCntxt_p->serialFd, TCSANOW, &(serialCntxt_p->dcb))) != 0)
   {
       printf("\ntcsetattr(%s) failed !! - errno<%d> \n",
              serDevName_p, errno);
       close(serialCntxt_p->serialFd);
       return -1;
   }

   // flush received data
   tcflush(serialCntxt_p->serialFd, TCIFLUSH);
   tcflush(serialCntxt_p->serialFd, TCOFLUSH);

   return 1;
}
P
#endif

                      
/*
 ********************************************************************
 *
 *
 *
 *
 ********************************************************************
 */
GW_xivelyDevInfo_s *GW_lookUpXivelyDev(unsigned char *extAddr_p)
{
   int idx, tblEntryCnt = sizeof(GW_xivelyInfoList)/sizeof(GW_xivelyDevInfo_s);

   for (idx=0; idx<tblEntryCnt; idx++)
   {
        if (memcmp(extAddr_p, GW_xivelyInfoList[idx].extAddr, LPWMN_MAC_EXT_ADDR_LEN) == 0x0)
        {
            return &(GW_xivelyInfoList[idx]);
        }
   }

   return NULL;
}

                          
/*
 ********************************************************************
 *
 *
 *
 *
 ********************************************************************
 */
GW_nodeChannInfo_s *GW_lookUpXivChann(GW_xivelyDevInfo_s *xivDev_p, int snsrId)
{
   int idx;

   for (idx=0; idx<GW_NODE_MAX_CHAN_CNT; idx++)
   {
       if (xivDev_p->nodeChannInfoList[idx].devId != 0x0)
       {
           if (snsrId == xivDev_p->nodeChannInfoList[idx].devId)
               return &(xivDev_p->nodeChannInfoList[idx]);
       }
       else
           break;
   }

   return NULL;
}


/*
 ********************************************************************
 *
 *
 *
 *
 ********************************************************************
 */
void GW_processEvt(unsigned char *buff_p, int msgLen)
{
   if (verbose) 
       printf("\nevtLen<%d>", msgLen);

   if (msgLen >= LPWMN_GW_EVT_ID_LEN)
   {
       unsigned int evtId = buff_p[0];

       msgLen -= LPWMN_GW_EVT_ID_LEN;
       buff_p += LPWMN_GW_EVT_ID_LEN;

       switch (evtId)
       {
          case LPWMN_GW_EVT_TYPE_NODE_REG:
               {
                  int idx;
                  printf("Received node registered event .... \n");

                  if (msgLen == (LPWMN_MAC_SHORT_ADDR_LEN 
                                 + LPWMN_MAC_EXT_ADDR_LEN
                                 + LPWMN_MAC_NODE_CAPABILITY_INFO_LEN))
                  {
                      unsigned int shortAddr;
                      LPWMN_nodeListEntry_s *node_p;


                      shortAddr = __ntohs(buff_p);
                      printf("Node short address <0x%x> \n", shortAddr);
                      buff_p += LPWMN_MAC_SHORT_ADDR_LEN;

                      printf("Ext addr: ");
                      for (idx=0; idx<LPWMN_MAC_EXT_ADDR_LEN; idx++)
                           printf("<0x%x> ", buff_p[idx]);
                      printf("\n");
                      
                      node_p = GW_lookUpShortAddr(shortAddr);
                      if (node_p != NULL)
                      {
                          printf("\nDup short address <0x%x> detected !!!!!! \n", shortAddr);
                          printf("\nNode with ext addr ");
                          for (idx=0; idx<LPWMN_MAC_EXT_ADDR_LEN; idx++)
                               printf("<0x%x>", node_p->extAddr[idx]);
                          printf("\n\n");
                          printf("\nQuitting ..................................... \n");
                          exit(1);
                      }

                      node_p = GW_lookUpExtAddr(buff_p);
                      if (node_p == NULL)
                      {
                          node_p = GW_allocNode();
                          memcpy(node_p->extAddr, buff_p, LPWMN_MAC_EXT_ADDR_LEN);
                      }

                      node_p->shortAddr = shortAddr;
                      node_p->regCnt ++;

                      // Is this node registered with Xively ?
                      node_p->xivDev_p = GW_lookUpXivelyDev(node_p->extAddr);
                      if (node_p->xivDev_p != NULL)
                      {
                          printf("This node is registered with Xively as <%s>\n",
                                 node_p->xivDev_p->xivDevName);  
                      }
                      else
                      {
                          printf("This node is not registered with Xively !!\n");
                      }
 
                      printf("Node reg count - %d\n", node_p->regCnt);

                      buff_p += LPWMN_MAC_EXT_ADDR_LEN;
                  }
               }
               break;

          case LPWMN_GW_EVT_TYPE_SYS_BOOT:
               {
                  printf("\nReceived LPWMN coord boot event .... \n");
               }
               break;
       }
   }
}


/*
 ********************************************************************
 *
 *
 *
 *
 ********************************************************************
 */
void GW_processNodeMsg(unsigned char *buff_p, int msgLen)
{
   int srcShortAddr, off = 0;
   LPWMN_nodeListEntry_s *node_p;
   int rssi, nodeRegd = 0;
   unsigned int lqi_corr;
   time_t frameRxTime;
   char *timeStr_p;


   time(&frameRxTime);
   timeStr_p = ctime(&frameRxTime);

   timeStr_p[strlen(timeStr_p) - 1] = '\0';

   if (msgLen < LPWMN_MAC_SHORT_ADDR_LEN + LPWMN_MSG_RSSI_LEN + LPWMN_MSG_CORR_LQI_LEN)
       return;

   srcShortAddr = buff_p[off];
   srcShortAddr = (srcShortAddr << 8) | buff_p[off + 1];

   GW_rcvdMsgCnt ++;

   node_p = GW_lookUpShortAddr(srcShortAddr);
   if (node_p == NULL)
   {
       printf("received message from unknown node <0x%x> -- dropping message \n", srcShortAddr);
   }
   else
       printf("[%u] rxd msg from node <0x%04x / 0x%02x:0x%02x> at <%s>", 
              GW_rcvdMsgCnt,
              srcShortAddr, 
              (unsigned int)node_p->extAddr[LPWMN_MAC_EXT_ADDR_LEN-2],
              (unsigned int)node_p->extAddr[LPWMN_MAC_EXT_ADDR_LEN-1],
              timeStr_p);  

   if (node_p != NULL)
   {
       node_p->msgRcvdCnt ++;

       // Check if this device is registered with Xively
       if (node_p->xivDev_p == NULL)
       {
           printf("\nThis node is not registered with Xively - !!");
       }
       else
           nodeRegd = 1;
   }

   // drop every other packet
   // if ((node_p->msgRcvdCnt % 5) != 0x0)
   //      return;

   off += LPWMN_MAC_SHORT_ADDR_LEN;

   rssi = (char)buff_p[off];
   lqi_corr = buff_p[off + 1];
 
   printf("    (rssi %d dBm, lqi %u)\n", (int)rssi, lqi_corr);
                       
   if (nodeRegd && sendToXively)
   { 
      xi_datapoint_t dataPoint;
      GW_nodeChannInfo_s *xivChannInfo_p;

      memset(&dataPoint, 0, sizeof(dataPoint));
      xi_set_value_i32(&dataPoint, rssi);
      
      dataPoint.timestamp.timestamp = time(NULL);
                          
      xivChannInfo_p = GW_lookUpXivChann(node_p->xivDev_p, 0xfe);
      if (xivChannInfo_p != NULL)
      {
          // Send RSSI to xively
          xi_datastream_update(node_p->xivDev_p->xiCntxt_p,
                               atoi(node_p->xivDev_p->feedString), 
                               xivChannInfo_p->xivelyChannId, 
                               &dataPoint);
      }
   }

   off += (LPWMN_MSG_RSSI_LEN + LPWMN_MSG_CORR_LQI_LEN);

   msgLen -= ( LPWMN_MAC_SHORT_ADDR_LEN + LPWMN_MSG_RSSI_LEN + LPWMN_MSG_CORR_LQI_LEN);

   if (msgLen < 1)
       return;

   if (verbose) 
       printf("\nDIS message type - 0x%x", buff_p[off]);
   off += DIS_MSG_TYPE_SZ;
   msgLen -= DIS_MSG_TYPE_SZ;
   buff_p += off;
   
   if (msgLen > 0xff)
       return;
   else
   {
       unsigned char rc, tlvLen1, *buff1_p;

       // hack !!!!!!!!!!!!!! 
       if (verbose) 
           printf("\ntlv type - 0x%x", *buff_p);

       *buff_p = DIS_TLV_TYPE_SENSOR_OUTPUT_LIST;

       rc = TLV_get(buff_p, msgLen, DIS_TLV_TYPE_SENSOR_OUTPUT_LIST, &tlvLen1, &buff1_p);
       if (rc == 0)
       {
           if (verbose) 
               printf("\nCould not find DIS_TLV_TYPE_SENSOR_OUTPUT_LIST !! \n");
           return;
       } 
       else
       {
           if (verbose)
               printf("\nFound DIS_TLV_TYPE_SENSOR_OUTPUT_LIST");

           while (1)
           {
              unsigned char tlvLen2, *buff2_p;

              rc = TLV_get(buff1_p, tlvLen1, DIS_TLV_TYPE_SENSOR_OUTPUT, &tlvLen2, &buff2_p);
              if (rc == 0)
              {
                  if (verbose)
                      printf("\nCould not find another DIS_TLV_TYPE_SENSOR_OUTPUT TLV !! \n");
                  break;
              } 
              else
              {
                  unsigned char tlvLen3, *buff3_p;
                  int snsrId, scaleFactor = DIS_DATA_SCALE_CENTI;
                  GW_nodeChannInfo_s *xivChannInfo_p = NULL;

                  buff1_p += (tlvLen2 + TLV_HDR_SZ);

                  if (verbose)
                      printf("\nFound DIS_TLV_TYPE_SENSOR_OUTPUT TLV .... val-fld-len<%d>", tlvLen2);
                  
                  rc = TLV_get(buff2_p, tlvLen2, DIS_TLV_TYPE_SENSOR_ID, &tlvLen3, &buff3_p);
                  if (rc == 0)
                      continue;
                  else
                  {
                      if (tlvLen3 == DIS_SENSOR_ID_FIELD_SZ) 
                      {
                          snsrId = *buff3_p;
                          if (verbose)
                              printf("\nSensor Id <0x%x>", snsrId);

                          // Check if this sensor is registered as a Xively channel

                          if (node_p != NULL)
                          {
                              xivChannInfo_p = GW_lookUpXivChann(node_p->xivDev_p, snsrId);
                              if (xivChannInfo_p == NULL)
                                 continue;
                              else
                              {
                                  printf("[%s]", xivChannInfo_p->xivelyChannId);
                              }
                          }
                      }
                      else
                         continue;
                  }

                  rc = TLV_get(buff2_p, tlvLen2, DIS_TLV_TYPE_DATA_SCALE_FACTOR, &tlvLen3, &buff3_p);
                  if (rc)
                  {
                      if (tlvLen3 == DIS_DATA_SCALE_FACTOR_FIELD_SZ)
                      {
                          scaleFactor = *buff3_p;
                          if (verbose)
                              printf("\nScale factor <%d>", scaleFactor); 
                          if (!(scaleFactor >= DIS_DATA_SCALE_TERA && scaleFactor <= DIS_DATA_SCALE_FEMTO))
                               scaleFactor = DIS_DATA_SCALE_NONE;
                      }
                  }

                  rc = TLV_get(buff2_p, tlvLen2, DIS_TLV_TYPE_VALUE, &tlvLen3, &buff3_p);
                  if (rc == 0)
                      continue;
                  else
                  {
                      int snsrOp;

                      if (verbose)
                          printf("\nFound DIS_TLV_TYPE_VALUE TLV .... val-fld-len<%d>", tlvLen3);
                
                      switch(tlvLen3)
                      {
                         case 1:
                             snsrOp = (int)(*buff3_p);
                             break;
                    
                         case 2:
                             snsrOp = (int)__ntohs(buff3_p);
                             break;

                         case 4:
                             snsrOp = (int)__ntohl(buff3_p);
                             break;

                         default:
                             break;
                      }

#if 1
                      if (nodeRegd && sendToXively == 1 && snsrOp == xivChannInfo_p->lastSnsrVal)
                      {
                          printf("\n ------------------------- Dropping data ------------------------------- !! \n");
                          continue;
                      }
#endif
                      if (xivChannInfo_p != NULL)
                          xivChannInfo_p->lastSnsrVal = snsrOp;

                      if (snsrId == 0x90)
                      {
                          if (snsrOp) 
                              snsrOp = 0x0;
                          else
                              snsrOp = 0x1;
                      }

                      if (xivChannInfo_p != NULL)
                      {
                         xi_datapoint_t dataPoint;
                         memset(&dataPoint, 0, sizeof(dataPoint));

                         switch (xivChannInfo_p->dataType)
                         {
                             case GW_XIV_CHANN_DATA_TYPE_FLOAT_32:
                                  {
                                      float valF = snsrOp;
                          
                                      switch (scaleFactor)
                                      {
                                          case DIS_DATA_SCALE_MICRO:
                                               valF /= 1000;
                                               valF /= 1000;
                                               break;

                                          case DIS_DATA_SCALE_MILLI:
                                               valF /= 1000;
                                               break;

                                          case DIS_DATA_SCALE_CENTI:
                                               valF /= 100;
                                               break;

                                          case DIS_DATA_SCALE_DECI:
                                               valF /= 10;
                                               break;

                                          default:
                                               break;
                                      }

                                      printf(" <%f> \n", valF);
                                      xi_set_value_f32(&dataPoint, valF);
                                  }
                                  break;

                             default:
                             case GW_XIV_CHANN_DATA_TYPE_INT_32:
                                  {
                                      printf(" <%d> \n", snsrOp);
                                      xi_set_value_i32(&dataPoint, snsrOp);
                                  }
                                  break;
                         } 


                         {
                             // Timestamp
                             time_t timer = 0;
                             time(&timer);
                             dataPoint.timestamp.timestamp = timer;
                         }


                         dataPoint.timestamp.timestamp = time(NULL);

                         if (verbose)
                             printf("\nChann Id - %s", xivChannInfo_p->xivelyChannId); 

                         switch (snsrId)
                         {
                             case 9:
                                  if (snsrOp == prevTemp)
                                      continue;
                                  prevTemp = snsrOp;
                                  break;

                             default:
                                  break;
                         }

#if 0

                         if (snsrId == 0x12)
                             printf("\n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ \n");

                         if (snsrId == 0xc0)
                         {
                             printf("\n Accel ---------------------------- 0x%x \n", snsrOp);
                             xi_set_value_i32(&dataPoint, ((snsrOp & 0x3) == 0x2) ? 0x1 : 0x0);
                             xi_datastream_update(node_p->xivDev_p->xiCntxt_p,
                                                  atoi(node_p->xivDev_p->feedString), 
                                                  "Orientation", 
                                                  &dataPoint);
                             snsrOp = (snsrOp >> 7) & 0x1;
                         }
#endif

                         if (nodeRegd && sendToXively)
                         {
                             xi_datastream_update(node_p->xivDev_p->xiCntxt_p,
                                                  atoi(node_p->xivDev_p->feedString), 
                                                  xivChannInfo_p->xivelyChannId, 
                                                  &dataPoint);

           
                             if (verbose)
                                 printf("\nXIV Update Rslt - Err: %d - %s\n", 
                                        (int)xi_get_last_error(), 
                                        xi_get_error_string(xi_get_last_error()));
                         }
                      }

                      if (verbose)
                          printf("\nSnsrOp<%d>", snsrOp);
                  }
              } 
          }
       }
   }
   return;
}


/*
 ********************************************************************
 *
 *
 *
 *
 ********************************************************************
 */
void GW_processRcvdMsg(unsigned char *buff_p, int offset, int gwMsgType, int msgLen)
{
    /*
     * GW payload format - 
     * Type (2 bytes)
     */ 

    switch (gwMsgType)
    {
       case LPWMN_GW_MSG_TYPE_RELAY_FROM_NODE:
            {
                printf("----------------------------------------------------------------------------------- \n");
                GW_processNodeMsg(buff_p + offset, msgLen); 
                printf("----------------------------------------------------------------------------------- \n");
            }
            break;

       case LPWMN_GW_MSG_TYPE_EVENT:
            {
                GW_processEvt(buff_p + offset, msgLen); 
            }
            break;

       default:
            break;
    }


    return;
}


/*
 ********************************************************************
 *
 *
 *
 *
 ********************************************************************
 */
void GW_deleteXivelyCntxt(void)
{
   int idx, tblEntryCnt = sizeof(GW_xivelyInfoList)/sizeof(GW_xivelyDevInfo_s);

   for (idx=0; idx<tblEntryCnt; idx++)
   {
       // delete the xi library context
       if (GW_xivelyInfoList[idx].xiCntxt_p != NULL)
           xi_delete_context(GW_xivelyInfoList[idx].xiCntxt_p);

   }
   return;
}


/*
 ********************************************************************
 *
 *
 *
 *
 ********************************************************************
 */
int GW_initXivelyIntf(void)
{
   int idx, tblEntryCnt = sizeof(GW_xivelyInfoList)/sizeof(GW_xivelyDevInfo_s);

   for (idx=0; idx<tblEntryCnt; idx++)
   {
       // create the xi library context
       GW_xivelyInfoList[idx].xiCntxt_p = xi_create_context(XI_HTTP,
                                                            GW_xivelyInfoList[idx].deviceKeyString,
                                                            atoi(GW_xivelyInfoList[idx].feedString));
       if (GW_xivelyInfoList[idx].xiCntxt_p == NULL)
       {
           printf("\nFailed to create Xively context for device <%s> !!!!!\n", 
                  GW_xivelyInfoList[idx].xivDevName);
           return -1;
       }
       else
       {
           printf("\nCreated Xively context for device <%s>\n", 
                  GW_xivelyInfoList[idx].xivDevName);
       }
   }

   printf("\nXively interface init done ...... \n");

   return 1; 
}


/*
 ********************************************************************
 *
 *
 *
 *
 ********************************************************************
 */
void __sendDummyData(int snsrOp)
{
   float valF = snsrOp;
   time_t timer = 0;
   LPWMN_nodeListEntry_s *node_p;
   static uint8_t eui64Addr[] = {0xfc, 0xc2, 0x3d, 0x0, 0x0, 0x0, 0xe8, 0x8f};
   GW_nodeChannInfo_s *xivChannInfo_p;
                          
   if (GW_initXivelyIntf() < 0)
   {
       printf("\nXively interface failure !! - quitting\n");
       return;
   }

   node_p = GW_allocNode();
   memcpy(node_p->extAddr, eui64Addr, LPWMN_MAC_EXT_ADDR_LEN);

   node_p->shortAddr = 0x123;
   node_p->regCnt ++;

   // Is this node registered with Xively ?
   node_p->xivDev_p = GW_lookUpXivelyDev(node_p->extAddr);
   
   xivChannInfo_p = &(node_p->xivDev_p->nodeChannInfoList[0]);
   
   xi_datapoint_t dataPoint;
   memset(&dataPoint, 0, sizeof(dataPoint));
                                      
   printf("\nData Type <Float32>, value<%f>", valF);
   xi_set_value_f32(&dataPoint, valF);
                         
   // Timestamp
   time(&timer);
   dataPoint.timestamp.timestamp = timer;
                         
   dataPoint.timestamp.timestamp = time(NULL);
   printf("\nChann Id - %s", xivChannInfo_p->xivelyChannId); 
                         
   xi_datastream_update(node_p->xivDev_p->xiCntxt_p,
                        atoi(node_p->xivDev_p->feedString), 
                        xivChannInfo_p->xivelyChannId, 
                        &dataPoint);

           
   printf("\nXIV Update Rslt - Err: %d - %s\n", 
          (int)xi_get_last_error(), 
          xi_get_error_string(xi_get_last_error()));
}

/*
 ********************************************************************
 *
 *
 *
 *
 ********************************************************************
 */
int main(int argc, const char* argv[] )
{
    int off = 0, readLen, pyldLen, totReadLen = 0;
    int currMsgType = 0xffff;

#ifdef XIVELY_TEST
    int sensorOp = 1;
    for (;;)
    {
       printf("\n ------------------ sensorOp <%d> ------------------ \n", sensorOp);
       __sendDummyData(sensorOp);
       sensorOp ++;
       if (sensorOp > 25)
           sensorOp = 1;
       sleep(1);  
    }
#endif

    if (argc < 2)
    {
        print_usage();
        return 1;
    }

    if (argc == 3)
        sendToXively = (argv[2][0] == 'n') ?  0x0 : 0x1; 

    printf("\nCalling cfgPort() ............... \n");
    if (cfgPort((char *)argv[1], B38400) < 0)
        return 2;
    printf("\ncfgPort() passed ............... \n");
    
    if (GW_initXivelyIntf() < 0)
    {
        printf("\nXively interface failure !! - quitting\n");
        return 3;
    }


    readLen = UART_MSG_HDR_PYLD_CRC_FIELD_OFF;

#ifdef __TEST__
    while (1)
    {
       int rc = readPort(serInputBuff, 1);
       if (rc != 1)
       {
           printf("\nreadPort() failed !! ................. \n");
           close(uart_cntxt.serialFd);
           break;
       }
       printf("\n <0x%x> ", serInputBuff[0]);
    }
#endif

    while (1)
    {
       int rc;

       if (verbose)
           printf("\noff<%d>/readLen<%d>/totReadLen<%d> \n", off, readLen, totReadLen);

       rc = readPort(serInputBuff + off, readLen);
       if (rc != readLen)
       {
           printf("\nreadPort() failed !! ................. \n");
           close(uart_cntxt.serialFd);
           break;
       }

       totReadLen += readLen;
       off += readLen;

       if (verbose)
           printf("\n rc<%d> \n", rc);

       switch (totReadLen)
       {
          case UART_MSG_HDR_PYLD_CRC_FIELD_OFF: 
               {
                   int idx;
                   unsigned short calcCrc16, rxdCrc16;

                   // Get the message length

                   if (verbose)
                       printf("\n-------------------------------------------------------------------------------------------------");

                   if (verbose)
                   {
                       printf("\nRead <%d> bytes --- ", readLen);
                       for (idx=0; idx<totReadLen; idx++)
                            printf(" <0x%x> ", serInputBuff[idx]);
                   }

                   calcCrc16 = crc16(serInputBuff, UART_MSG_HDR_HDR_CRC_FIELD_OFF);
                   rxdCrc16 = serInputBuff[UART_MSG_HDR_HDR_CRC_FIELD_OFF];
                   rxdCrc16 = (rxdCrc16 << 8) + serInputBuff[UART_MSG_HDR_HDR_CRC_FIELD_OFF + 1];
                   if (verbose)
                       printf("\ncalc-crc16<0x%x> rcvd-crc16<0x%x>\n", calcCrc16, rxdCrc16);

                   if (calcCrc16 != rxdCrc16)
                   {
                       for (idx=0; idx<UART_MSG_HDR_PYLD_CRC_FIELD_OFF-1; idx++)
                            serInputBuff[idx] = serInputBuff[idx+1];
                       off = UART_MSG_HDR_PYLD_CRC_FIELD_OFF - 1;
                       readLen = 1;
                       totReadLen = off;
                       break;
                   }

                   pyldLen = serInputBuff[UART_MSG_HDR_PYLD_LEN_FIELD_OFF];
                   pyldLen = (pyldLen << 8) |  serInputBuff[UART_MSG_HDR_PYLD_LEN_FIELD_OFF + 1];

                   currMsgType = serInputBuff[UART_MSG_HDR_MSG_TYPE_FIELD_OFF];
                   currMsgType = (currMsgType << 8) | serInputBuff[UART_MSG_HDR_MSG_TYPE_FIELD_OFF + 1];

                   if (verbose)
                       printf("\nMessage Type<%d> / Length<%d>", currMsgType, pyldLen);                   

                   readLen = pyldLen + UART_MSG_HDR_PYLD_CRC_FIELD_LEN;
               }
               break;

          default:
               {
                   GW_processRcvdMsg(serInputBuff, UART_MSG_HDR_LEN, currMsgType, pyldLen);
                   readLen = UART_MSG_HDR_PYLD_CRC_FIELD_OFF;
                   totReadLen = 0;
                   off = 0;
               }
               break;
       }
    }

    GW_deleteXivelyCntxt();

    return 0;
}
